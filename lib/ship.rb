class Ship
  attr_accessor :positions
  attr_reader :name, :length

  def initialize(name, length)
    @name = name
    @length = length
  end

  def positions(start_tile, end_tile)
    positions = Array.new(self.length) { Array.new(2) }
    row_pos_ar = (start_tile[0]..end_tile[0]).to_a
    col_pos_ar = (start_tile[1]..end_tile[1]).to_a

    @positions = positions.map.with_index do |row, idx|
      row_pos_ar.length > 1 ? row_pos = row_pos_ar[idx] : row_pos = row_pos_ar[0]
      col_pos_ar.length > 1 ? col_pos = col_pos_ar[idx] : col_pos = col_pos_ar[0]
      [row_pos, col_pos]
    end
  end

  def valid_positions?(positions, board)
    positions.map { |position| valid_postion?(position, board) }.all?
  end

  def valid_postion?(position, board)
    board.in_range?(position) && position.all? && board[position].nil?
  end

  def sunk?(board)
    @positions.map { |pos| board[pos] == :x ? true : false }.all?
  end
end
