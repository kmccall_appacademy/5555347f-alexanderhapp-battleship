class Board
  attr_reader :grid

  def self.default_grid
    Array.new(10) { Array.new(10) }
  end

  def initialize(grid = Board.default_grid)
    @grid = grid
  end

  def [](pos)
    row, col = pos
    @grid[row][col]
  end

  def []=(pos, mark)
    row, col = pos
    @grid[row][col] = mark
  end

  def count
    @grid.map do |row|
      row.map { |pos| pos == :s ? 1 : 0 }.reduce(:+)
    end.reduce(:+)
  end

  def empty?(pos = nil)
    if pos.nil?
      @grid.each { |row| return false if row.include?(:s) }
      true
    else
      self[pos].nil?
    end
  end

  def full?
    @grid.each do |row|
      row.each { |el| return false if el.nil? }
    end
    true
  end

  def in_range?(pos)
    (0...@grid.length).cover?(pos[0]) && (0...@grid.length).cover?(pos[1])
  end

  def place_mark(pos, mark)
    self[pos] = mark if in_range?(pos)
  end

  def place_ship_tile(pos)
    place_mark(pos, :s)
  end

  def place_random_ship
    if self.full?
      (raise "board full")
    else
      pos = random_position
      self.place_mark(pos, :s) if self.empty?(pos)
    end
  end

  def destroyed?(pos)
    self[pos] = :x
  end

  def hit?(pos)
    self[pos] == :s
  end

  def populate_grid
    until self.count == 10
      place_random_ship
    end
  end

  def length
    self.grid.length
  end

  def won?
    self.empty?
  end

  def random_position
    [rand(@grid.length), rand(@grid.length)]
  end

  def place_random_x
    self.place_mark(random_position, :x)
  end

  def hit_count
    @grid.map do |row|
      row.map { |pos| pos == :x ? 1 : 0 }.reduce(:+)
    end.reduce(:+)
  end

  def populate_with_hits(number_of_hits)
    until self.hit_count == number_of_hits
      place_random_x
    end
  end

  def display_board_with_ships
    print "\n  ".ljust(5)
    self.grid.each_index { |idx| print idx.to_s.ljust(4) }
    puts "\n"
    print_row_of_dashes
    self.grid.each_with_index do |row, idx|
      print idx.to_s.ljust(2) + "|".ljust(2)
      row.each do |el|
        print el.to_s.ljust(2)
        print "|".ljust(2)
      end
      puts "\n"
      print_row_of_dashes
    end
    puts "\n"
  end

  def display_board_without_ships
    print "\n  ".ljust(5)
    self.grid.each_index { |idx| print idx.to_s.ljust(4) }
    puts "\n"
    print_row_of_dashes
    self.grid.each_with_index do |row, idx|
      print idx.to_s.ljust(2) + "|".ljust(2)
      row.each do |el|
        el == :s ? (print nil.to_s.ljust(2)) : (print el.to_s.ljust(2))
        print "|".ljust(2)
      end
      puts "\n"
      print_row_of_dashes
    end
    puts "\n"
  end

  def print_row_of_dashes
    print "----".ljust(4) * self.grid.length
    puts "--\n"
  end

end
