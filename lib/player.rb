class Player
  attr_accessor :board
  attr_reader :name, :carrier, :battleship, :submarine, :cruiser, :destroyer

  def initialize(name, board = Board.new)
    @name = name
    @board = board
    @carrier = Ship.new("Aircraft Carrier", 5)
    @battleship = Ship.new("Battleship", 4)
    @submarine = Ship.new("Submarine", 3)
    @cruiser = Ship.new("Cruiser", 3)
    @destroyer = Ship.new("Destroyer", 2)
    @ships = [carrier, battleship, submarine, cruiser, destroyer]
  end

  def place_ship(positions)
    positions.each { |position| @board.place_ship_tile(position) }
  end

  def print_row_of_dashes
    print "----".ljust(4) * @board.grid.length
    puts "--\n"
  end
end

class HumanPlayer < Player

  def get_play
    puts "#{@name}, where would you like to move?"
    get_input
  end

  def setup_board
    self.display_board
    @ships.each do |ship|
      start_tile, end_tile = get_start_and_end_tiles(ship)
      positions = ship.positions(start_tile, end_tile)
      unless ship.valid_positions?(positions, @board)
        puts "\nInvalid position. Try again."
        redo
      end
      place_ship(positions)
      self.display_board
    end
    nil
  end

  def display_board
    print "\n  ".ljust(5)
    @board.grid.each_index { |idx| print idx.to_s.ljust(4) }
    puts "\n"
    print_row_of_dashes
    @board.grid.each_with_index do |row, idx|
      print idx.to_s.ljust(2) + "|".ljust(2)
      row.each do |el|
        print el.to_s.ljust(2)
        print "|".ljust(2)
      end
      puts "\n"
      print_row_of_dashes
    end
    puts "\n"
  end

  def get_start_and_end_tiles(ship)
    puts "\n#{ship.name}. Length: #{ship.length}\n"
    puts "\nEnter start position: "
    start_tile = get_input
    puts "\nEnter end position: "
    end_tile = get_input
    [start_tile, end_tile]
  end

  def get_input
    loop do
      input = gets.chomp.split(",").map(&:to_i)
      return input if input.count == 2
    end
  end

end

class ComputerPlayer < Player

  def setup_board
    @ships.each do |ship|
      start_tile, end_tile = get_start_and_end_tiles(ship)
      positions = ship.positions(start_tile, end_tile)
      unless ship.valid_positions?(positions, @board)
        puts "\nInvalid position. Try again."
        redo
      end
      place_ship(positions)
    end
    nil
  end

  def get_play
    loop do
      move = [rand(@board.length), rand(@board.length)]
      return move unless @board[move] == :x
    end
  end

  def display_board
    print "\n  ".ljust(5)
    @board.grid.each_index { |idx| print idx.to_s.ljust(4) }
    puts "\n"
    print_row_of_dashes
    @board.grid.each_with_index do |row, idx|
      print idx.to_s.ljust(2) + "|".ljust(2)
      row.each do |el|
        el == :s ? (print nil.to_s.ljust(2)) : (print el.to_s.ljust(2))
        print "|".ljust(2)
      end
      puts "\n"
      print_row_of_dashes
    end
    puts "\n"
  end

  def get_start_and_end_tiles(ship)
    end_tile = nil
    until end_tile
      start_tile = [rand(@board.length), rand(@board.length)]
      vertical_end_tile = [start_tile[0] + ship.length - 1, start_tile[1]]
      horizontal_end_tile = [start_tile[0], start_tile[1] + ship.length - 1]

      if ship.valid_positions?([horizontal_end_tile, vertical_end_tile], @board)
        end_tile = [horizontal_end_tile, vertical_end_tile].shuffle.first
      elsif ship.valid_postion?(horizontal_end_tile, @board)
        end_tile = horizontal_end_tile
      elsif ship.valid_postion?(vertical_end_tile, @board)
        end_tile = vertical_end_tile
      end
    end
    [start_tile, end_tile]
  end

end
