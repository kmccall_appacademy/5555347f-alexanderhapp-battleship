require_relative 'ship'
require_relative 'player'
require_relative 'board'

class BattleshipGame
  attr_reader :board, :player_one, :player_two, :offensive_player, :defensive_player

  def initialize(player_one, board = Board.new, player_two = ComputerPlayer.new("sam"))
    @player_one = player_one
    @player_two = player_two
    @active_player = player_one
    @inactive_player = player_two
    @board = board
  end

  def attack(pos)
    @board.place_mark(pos, :x)
  end

  def count
    @board.count
  end

  def game_over?
    @board.won?
  end

  def switch_players!
    @active_player, @inactive_player = @inactive_player, @active_player
  end

  def setup
    puts "\nPlease place your ships on the board.\n"
    [@player_one, @player_two].each(&:setup_board)
  end

  def play_turn
    move = @active_player.get_play
    if @board.hit?(move) ? (puts "\nHit!") : (puts "\nMiss!")
    attack(move)
    switch_players!
  end

  def play
    @board = @active_player.board
    display_greeting
    setup
    until game_over?
      @board = @inactive_player.board
      play_turn
      display_status
    end
    puts "\nYou win! Congratulations, #{@inactive_player.name.capitalize}"
  end

  def display_status
    puts "\nShips remaining: #{count}"
    @active_player.display_board
  end

  def display_greeting
    puts "\n       WELCOME TO BATTLESHIP!\n"
  end

  if __FILE__ == $PROGRAM_NAME
    alex = HumanPlayer.new("alex")

    game = BattleshipGame.new(alex)

    game.play
  end
end
